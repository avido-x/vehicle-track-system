// 用户路由模块，设置用户登录/注册的路由访问路径
// tips: 目前只用邮箱作为登录凭证，后续增加手机号等
const express = require('express')
const User = require('../../models/User')
const router = express.Router()
const bcrypt = require('bcrypt')  // 通过bcrypt加密
const jwt = require('jsonwebtoken')
const keys = require('../../config/keys')
const passport = require('passport')

// $route GET api/users/test
// @desc 返回请求的json数据
// @access public
router.get('/test', (req, res) => {
    res.json('test register/login router.')
})

// $route POST api/users/register
// @desc 注册接口，返回json数据（注册参数）
// @access public
router.post('/register', (req, res) => {
    // 用户赋值递增user_id，第一个为22000001
    // 暂未考虑多线程情况
    let num;
    User.findOne({}).sort({_id: -1}).limit(1).exec((err, docs) => {
        num = docs.user_id ? docs.user_id+1 : 20220406
    })
    // 验证该邮箱是否被注册
    User.findOne({ email: req.body.email })
        .then(user => {
            if(user) {
                return res.status(400).json('该邮箱已被注册!')
            } else {
                const newUser = new User({
                    user_id: num,
                    name: req.body.name,
                    email: req.body.email,
                    phone: req.body.phone,
                    password: req.body.phone
                })
                // 使用bcrypt加密
                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(newUser.password, salt, (err, hash) => {
                        if(err) throw err
                        // 将用户的密码换成加密后的
                        newUser.password = hash
                        // 将用户的信息存储到数据库中
                        newUser.save()
                            .then(user => { res.json(user) })  // 存储成功即返回用户信息
                            .catch(err => { console.log(err); })
                    })
                })
            }
        })
})

// $route POST api/users/login
// @desc 返回token('Bearer ' + token)
// @access public
router.post('/login', (req, res) => {
    const email = req.body.email
    const password = req.body.password

    User.findOne({ email })
        .then(user => {
            if(!user) return res.status(404).json('用户不存在')

            // 密码匹配，bcrypt的compare函数
            bcrypt.compare(password, user.password)
                .then(isMatch => {
                    // 密码正确，获取token
                    // jsonwebtoken.sign('加密规则', '加密key', 'token过期时间', comeback)
                    if(isMatch) {
                        const rule = {
                            id: user._id,
                            name: user.name,
                            phone: user.phone,
                            identity: user.identity
                        }
                        jwt.sign(rule, keys.secretOrKey, { expiresIn: 3600 }, (err, token) => {
                            if(err) throw err
                            res.json({
                                success: true,
                                token: 'Bearer ' + token
                            })
                        })
                    }
                    else return res.status(400).json('密码错误')
                })
        })
})

// $route POST api/users/current
// @desc 返回用户信息(用户携带Token才能访问)
// @access private
router.get('/current', passport.authenticate("jwt", { session: false }), (req, res) => {
    res.json({
        id: req.user.id,
        user_id: req.user.user_id,
        name: req.user.name,
        email: req.user.email,
        phone: req.user.phone,
        identity: req.user.identity
    })
})

// $route POST api/users/identity
// @desc 过滤指定身份的用户信息
// @access private
router.post('/identity',passport.authenticate("jwt", { session: false }), (req, res) => {
    // 是否判断当使用者身份为manager时才可过滤
    const identity = req.body.identity
    User.find({ identity })
        .then(users => {
            if(!users) res.json({})
            res.json(users)
        })
})

// $route POST api/users/edit
// @desc 修改用户信息，包括身份改动
// @access private
router.post('/edit/:id', passport.authenticate("jwt",{session: false}), (req,res) => {
    const userFileds = {}
    if(req.body.user_id) userFileds.user_id = req.body.user_id
    if(req.body.name) userFileds.name = req.body.name
    if(req.body.email) userFileds.email = req.body.email
    if(req.body.phone) userFileds.phone = req.body.phone
    if(req.body.identity) userFileds.identity = req.body.identity

    User.findOneAndUpdate(
        {_id: req.params.id},
        {$set: userFileds},
        {new: true  }
    )
        .then(user => res.json(user))
})

// $route POST api/users/delete/:id
// @desc 返回删除的用户信息
// @access Private
router.post('/delete/:id',passport.authenticate("jwt", {session: false}), (req,res) => {
    User.findOneAndRemove({_id: req.params.id})
        .then(user => {  // 删除后找不到该vehicle，该返回应有误。
            // user.save()
            //     // .then(newVehicle => res.json(newVehicle)) // 返回内容待查验
            //     .then(newVehicle => res.json('删除成功!'))
            res.json('删除成功')
        })
        .catch(err => res.status(404).json('删除失败!'))
})

module.exports = router