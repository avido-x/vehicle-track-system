// vehicle_id, plates_num, gps_lat, gps_lng, gps_date, gps_time, driver, driver_phone,path, status
// 车辆ID，车牌号，纬度，经度，日期，时间，驾驶人，驾驶人电话，路径，车辆状态

const express = require('express')
const Gps = require('../../models/Gps')
const router = express.Router()
const passport = require('passport')

// $route GET api/gps/test
// @desc 返回请求的json数据
// @access public
router.get('/test', (req, res) => {
    res.json('gps api test.')
})

// $route GET api/gps
// @desc 返回请求的json数据
// @access private
router.get('/', passport.authenticate('jwt',{session: false}), (req, res) => {
    Gps.find()
        .then(gps => {
            if(!gps) res.status(404).json('暂无数据')
            return res.json(gps)
        })
        .catch(err => {res.status(404).json(err)})
})

// $route POST api/gps/submit
// @desc 返回请求的json数据
// @access private
router.post('/submit',passport.authenticate('jwt', {session: false}), (req, res) => {
    const gpsFields = {}
    if(req.body.vehicle_id) gpsFields.vehicle_id = req.body.vehicle_id
    if(req.body.plates_num) gpsFields.plates_num = req.body.plates_num
    if(req.body.gps_lat) gpsFields.gps_lat = req.body.gps_lat
    if(req.body.gps_lng) gpsFields.gps_lng = req.body.gps_lng
    if(req.body.gps_date) gpsFields.gps_date = req.body.gps_date
    if(req.body.gps_time) gpsFields.gps_time = req.body.gps_time
    if(req.body.driver) gpsFields.driver = req.body.driver
    if(req.body.driver_phone) gpsFields.driver_phone = req.body.driver_phone
    if(req.body.status) gpsFields.status = req.body.status
    if(req.body.path) gpsFields.path = req.body.path
    
    new Gps(gpsFields).save()
        .then(gps => {
            res.json(gps)
        })
})

// $route POST api/gps/current
// @desc 获取gps数据模型最新一条，并将日期时间修改为当前，返回更新后的数据
// @access private
router.post('/current', passport.authenticate('jwt', {session: false}), (req,res) => {
    let date = new Date()
    let now_date = date.getFullYear() + "-" +(date.getMonth() + 1) +"-" +date.getDate()
    let now_time = date.getHours() + ":" + date.getMinutes()
    let new_data = {
        gps_date: now_date,
        gps_time: now_time
    }
    let id;
    let driver = req.body.driver
    Gps.findOne({ driver }).sort({_id: -1}).limit(1).exec((err, docs) => {
        id = docs._id
        Gps.findByIdAndUpdate(
            {_id: id},
            {$set: new_data},
            {new: true}
        )
            .then(newGps => {res.json(newGps)})
    })
    
})

module.exports = router