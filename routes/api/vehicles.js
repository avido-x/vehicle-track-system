// 车辆信息模块，展示所有车辆信息
// id 车牌 车型 颜色 适用路线 可申请时间段 车辆起点 车辆状态
const express = require('express')
const Vehicle = require('../../models/Vehicle')
const router = express.Router()
const passport = require('passport')
const { isRegExp } = require('util/types')
const { equal } = require('assert')
const { json } = require('stream/consumers')

// $route GET api/vehicles/test
// @desc 返回请求的json数据
// @access public
router.get('/test', (req, res) => {
    res.json('vehicle api test.')
})

// $route POST api/vehicles/add
// @desc 返回添加的车辆信息
// @access private
router.post('/add', passport.authenticate('jwt',{ session: false }) ,(req,res) => {
    const vehicleFields = {}
    if(req.body.vehicle_id) vehicleFields.vehicle_id = req.body.vehicle_id
    if(req.body.plates_num) vehicleFields.plates_num = req.body.plates_num
    if(req.body.model) vehicleFields.model = req.body.model
    if(req.body.color) vehicleFields.color = req.body.color
    if(req.body.route) vehicleFields.route = req.body.route
    if(req.body.time) vehicleFields.time = req.body.time
    if(req.body.origin) vehicleFields.origin = req.body.origin
    if(req.body.status) vehicleFields.status = req.body.status

    new Vehicle(vehicleFields).save()
        .then(vehicle => {
            res.json(vehicle)
        })
})

// $route GET api/vehicles
// @desc 返回信息的json数据
// @access Provate
router.get('/', passport.authenticate("jwt", {session: false}), (req,res) => {
    Vehicle.find()
        .then(vehicle => {
            if(!vehicle) return res.status(404).json('暂无数据')
            return res.json(vehicle)
        })
        .catch(err => res.status(404).json(err))
})

// $route POST api/vehicles/edit
// @desc 返回更新后的车辆信息
// @access Private
router.post('/edit/:id', passport.authenticate("jwt", {session: false}), (req,res) => {
    const vehicleFields = {}
    if(req.body.vehicle_id) vehicleFields.vehicle_id = req.body.vehicle_id
    if(req.body.plates_num) vehicleFields.plates_num = req.body.plates_num
    if(req.body.model) vehicleFields.model = req.body.model
    if(req.body.color) vehicleFields.color = req.body.color
    if(req.body.route) vehicleFields.route = req.body.route
    if(req.body.time) vehicleFields.time = req.body.time
    if(req.body.origin) vehicleFields.origin = req.body.origin
    if(req.body.status) vehicleFields.status = req.body.status

    Vehicle.findOneAndUpdate(
        {_id: req.params.id},
        {$set: vehicleFields},
        {new: true}
    )
        .then(vehicle => res.json(vehicle))
})

// $route POST api/vehicles/delete/:id
// @desc 返回删除的车辆信息
// @access Private
router.post('/delete/:id',passport.authenticate("jwt", {session: false}), (req,res) => {
    Vehicle.findOneAndRemove({_id: req.params.id})
        .then(vehicle => {  // 删除后找不到该vehicle，该返回应有误。
            res.json('删除成功')
            // vehicle.save()
                // .then(newVehicle => res.json(newVehicle)) // 返回内容待查验
                // .then(newVehicle => res.json('删除成功!'))
        })
        .catch(err => res.status(404).json('删除失败!'))
})
module.exports = router