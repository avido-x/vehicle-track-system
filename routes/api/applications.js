// vehicle_id, plates_num, date, time, driver, driver_phone, status, application_status
// 车辆ID，车牌号，申请日期，申请时间段，申请人，申请人号码，车辆状态，申请状态

const express = require('express')
const Application = require('../../models/Application')
const router = express.Router()
const passport = require('passport')

// $route GET api/applications/test
// @desc 返回请求的json数据
// @access public
router.get('/test', (req, res) => {
    res.json('application api test.')
})

// $route POST api/applications/info
// @desc 返回生成的车辆申请信息
// @access private
router.post('/info',passport.authenticate('jwt', {session: false}), (req, res) => {
    const applicationFields = {}
    if(req.body.vehicle_id) applicationFields.vehicle_id = req.body.vehicle_id
    if(req.body.plates_num) applicationFields.plates_num = req.body.plates_num
    if(req.body.date) applicationFields.date = req.body.date
    if(req.body.time) applicationFields.time = req.body.time
    if(req.body.driver) applicationFields.driver = req.body.driver
    if(req.body.driver_phone) applicationFields.driver_phone = req.body.driver_phone
    if(req.body.status) applicationFields.status = req.body.status
    if(req.body.application_status) applicationFields.application_status = req.body.application_status
    
    new Application(applicationFields).save()
        .then(application => {
            res.json(application)
        })
})

// $route GET api/applications/
// @desc 返回获取的车辆申请
// @access private
router.post('/', passport.authenticate('jwt', {session: false}), (req,res) => {
    if(req.body.driver) {
        const driver = req.body.driver 
        Application.find({driver})
        .then(application => {
            if(!application) return res.status(404).json('暂无数据')
            return res.json(application)
        })
        .catch(err => res.status(404).json(err))
    } else {
        Application.find()
        .then(application => {
            if(!application) return res.status(404).json('暂无数据')
            return res.json(application)
        })
        .catch(err => res.status(404).json(err))  
    }
   
})

// $route POST api/applications/cancel/:id
// @desc 返回删除成功
// @access private
router.post('/cancel/:id',passport.authenticate('jwt', {session: false}),(req,res) => {
    Application.findByIdAndRemove({_id: req.params.id})
        .then(application => {
            res.json('撤销成功')
        })
})

// $route POST api/applications/edit/:id
// @desc 返回修改后的申请
// @access private
router.post('/edit/:id', passport.authenticate('jwt', {session: false}), (req, res) => {
    const applicationFields = {}
    if(req.body.vehicle_id) applicationFields.vehicle_id = req.body.vehicle_id
    if(req.body.plates_num) applicationFields.plates_num = req.body.plates_num
    if(req.body.date) applicationFields.date = req.body.date
    if(req.body.time) applicationFields.time = req.body.time
    if(req.body.driver) applicationFields.driver = req.body.driver
    if(req.body.driver_phone) applicationFields.driver_phone = req.body.driver_phone
    if(req.body.status) applicationFields.status = req.body.status
    if(req.body.application_status) applicationFields.application_status = req.body.application_status

    Application.findByIdAndUpdate(
        {_id: req.params.id},
        {$set: applicationFields},
        {new: true}
    )
        .then(application => {
            res.json(application)
        })
})


module.exports = router