import service from '../utils/request'

// 获取所有车辆信息
export const getVehicles = () => {
    return service({
        url: '/api/vehicles',
        method: 'GET'
    })
}

// 添加车辆信息
export const addVehicle = (data: object) => {
    return service({
        url: '/api/vehicles/add',
        method: 'POST',
        data
    })
}

// 编辑车辆信息
export const editVehicle = (id: string, data: object) => {
    return service({
        url: `/api/vehicles/edit/${id}`,  
        method: 'POST',
        data
    })
}

// 删除车辆信息
export const removeVehicle = (id: string) => {
    return service({
        url: `/api/vehicles/delete/${id}`,
        method: 'POST'
    })
} 

