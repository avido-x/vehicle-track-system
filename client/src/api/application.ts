import service from "../utils/request";

// 获取所有从车辆申请数据
export const getApplications = (data:object) => {
    return service({
        url: "/api/applications",
        method: 'POST',
        data
    })
}

// 提交车辆申请
export const submitApplication = (data: object) => {
    return service({
        url: '/api/applications/info',
        method: 'POST',
        data
    })
}

// 撤销车辆申请
export const cancelApplication = (id: string) => {
    return service({
        url: `/api/applications/cancel/${id}`,
        method: 'POST'
    })
}

// 编辑车辆申请（可用于审核车辆申请）
export const editApplication = (id: string, data: object) => {
    return service({
        url: `/api/applications/edit/${id}`,
        method: 'POST',
        data
    })
} 