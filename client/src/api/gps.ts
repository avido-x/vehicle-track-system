import service from "../utils/request";

export const submitGps = (data: object) => {
    return service({
        url: '/api/gps/submit',
        method: 'POST',
        data
    })
}

export const currentGps = (data: object) => {
    return service({
        url: '/api/gps/current',
        method: 'POST',
        data
    })
}