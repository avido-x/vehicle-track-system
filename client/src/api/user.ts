import service from '../utils/request'

// 注册
export const register = (data: object) => {
    return service({
        url: '/api/users/register',
        method: 'POST',
        data
    })
}

// 登录
export const login = (data: object) => {
    return service({
        url: '/api/users/login',
        method: 'POST',
        data
    })
}

// 过滤指定身份的用户
export const filterUsers = (data: object) => {
    return service({
        url: '/api/users/identity',
        method: 'POST',
        data
    })
}

// 查看当前用户信息
export const getCurrentUser = () => {
    return service({
        url: '/api/users/current',
        method: 'GET'
    })
}

// 修改用户信息
export const editUserInfo = (id: string, data: object) => {
    return service({
        url: `api/users/edit/${id}`,
        method: 'POST',
        data
    })
}

// 删除用户信息
export const deleteUserInfo = (id: string) => {
    return service({
        url: `api/users/delete/${id}`,
        method: 'POST'
    })
}