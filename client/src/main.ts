import { createApp } from 'vue'
import App from './App.vue'
import router from './modules/router'
import pinia from './modules/pinia'
// import BaiduMap from 'vue-baidu-map'  // 引入百度地图api

const app = createApp(App)
// app.config.devtools = true;

// 配置全局属性$axios
// app.config.globalProperties.$axios = axios
// 存在问题，待解决：封装request.ts,整合请求的api
// declare module '@vue/runtime-core' {
//     interface ComponentCustomProperties {
//         $axios: AxiosInstance
//     }
// }

app.use(router)
app.use(pinia)
// app.use(BaiduMap, {
//     ak: '8tRsM5r8lrjw4H2urInWPuRsLiv38XsU'
// })app.use(BaiduMap, {
//     ak: '8tRsM5r8lrjw4H2urInWPuRsLiv38XsU'
// })
app.mount('#app')
 