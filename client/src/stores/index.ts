import { defineStore } from "pinia";

export const userStore = defineStore('user', {
    state() {
        return {
            isAuthenticated: false,  // 是否授权
            user: {}
        }
    },
    actions: {
        // 设置是否授权的值
        setAuthenticated(isAuthenticated:boolean) {
            this.updateAuthenticated(isAuthenticated)
        },
        updateAuthenticated(val: boolean) {
            this.isAuthenticated = val
        },
        setUser(user: object) {
            this.updateuUser(user)
        },
        updateuUser(val: object) {
            this.user = val   // 对象赋值？待修改
        },
        // 清除当前的登录状态及用户信息，待优化
        clearCurrentState() {
            this.updateAuthenticated(false)
            this.updateuUser({})
        }
    }
})