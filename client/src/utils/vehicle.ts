// 1. 获取当前时间，随机生成经纬度
// 我国经纬度范围：经度73.33-135.05  纬度3.51-53.33(lat)
export const getRandomData = () => {
    const minLat = 25.51,maxLat = 30.33, minLng = 80.33 ,maxLng = 120.05;
    let lat = Math.random() * (maxLat - minLat) + minLat;
    let lng = Math.random() * (maxLng - minLng) + minLng;

    const map_data = {
        map_lat: lat,
        map_lng: lng
    }
    console.log(map_data)
    return map_data
}


// 2. 三分钟内在指定范围内更新一次经纬度
// 参数为当前经纬度，根据该经纬度在0.1/0.12内变化（可匀速？）
function getRandomPath() {

}