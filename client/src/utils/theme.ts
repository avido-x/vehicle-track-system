// 修改主题色
import type { GlobalThemeOverrides } from "naive-ui";

export const themeOverrides: GlobalThemeOverrides = {
  common: {
    // primaryColor: "#676e8a",
    primaryColor: "#fff",
    primaryColorSuppl: '#fff',
    primaryColorHover: "#fff",
    primaryColorPressed: "#fff",
  }
};
