import { h } from "vue";
import { NIcon, NTag } from "naive-ui";

// 判空函数
export const isEmpty = (value: any) => {
  return (
    value === undefined ||
    value === null ||
    (typeof value === "object" && Object.keys(value).length === 0) ||
    (typeof value === "string" && value.trim().length === 0)
  );
};

// 图标渲染
export const renderIcon = (icon: any) => {
  return () => h(NIcon, null, { default: () => h(icon) });
};

// tag渲染
const TagColors = {
  color: "#676E8A",
  textColor: "#fff",
  borderColor: "#676E8A",
};
export const renderTag = (
  type = "warning",
  text = "New",
  color: object = TagColors
) => {
  return () => {
    h(
      NTag as any,
      {
        type,
        round: true,
        size: "small",
        color,
      },
      { default: () => text }
    );
  };
};
