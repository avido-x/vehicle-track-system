import axios from "axios";
import router from "../modules/router";

// 创建axios实例
const service = axios.create({
  // baseURL: "http://localhost:5000",  // 跨域已设置访问路径，不需要baseUrl
});

// 请求拦截
service.interceptors.request.use(
  (config: any) => {
    window.$loading.start();
    if (localStorage.aToken) {
      // 请求头携带token，确保调接口时身份认证
      config.headers.Authorization = localStorage.aToken;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

// 响应拦截
service.interceptors.response.use(
  (res: any) => {
    window.$loading.finish();
    return res
  },
  (error) => {
    window.$loading.finish();
    window.$message.error(error.response.data);
    const { status } = error.response;
    if(status === 401) {
      window.$message.error('会话已过期，请重新登录')
      localStorage.removeItem('aToken')
      localStorage.removeItem('user')
      router.push('/login')
    }
    return Promise.reject(error);
  }
);

export default service;
