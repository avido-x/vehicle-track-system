function formatRouterTree(data: any) {
  // 区别一级路由和二级路由
  let parents = data.filter((item) => item.pid === 0);
  let children = data.filter((item) => item.pid !== 0);

  dataToTree(parents, children);

  // 根据一级路由和二级路由，生成树形结构
  function dataToTree(parents: any, children: any) {
    parents.map((p) => {
      children.map((item, index) => {
        if (item.pid === p.id) {
          // 判断是否为父子路由
          let _c = JSON.parse(JSON.stringify(children)); // 确保对象形式？
          // splice会改变原始数组
          _c.splice(index, 1); // 操作完后删除该item

          // dataToTree([item],_c)  // 只存在二级路由，该函数无意义
          if (p.children) {
            p.children.push(item);
          } else {
            p.children = [item];
          }
        }
      });
    });
  }
  return parents;
}

// 生成菜单
function generateRouter(usersRouters) {
  let newRouters = usersRouters.map((item) => {
    let routes = {
    //   path: item.path,
    //   name: item.name,
    //   component: () => import(`../pages${item.link}.vue`),
    label: item.title,
    key: item.name
    };
    if (item.children) {
      routes.children = generateRouter(item.children);
    }
    return routes;
  });
  return newRouters;
}

export { formatRouterTree, generateRouter };
