// 获取路由，主要针对index路由设置
function getBasicRoutes(identity: string) {
  const basicRoutes = [
    {
      path: "/",
      redirect: "/login",
    },
    {
      name: "测试",
      path: "/test",
      component: () => import("../pages/test.vue"),
    },
    {
      name: "注册",
      path: "/register",
      component: () => import("../pages/Register.vue"),
    },
    {
      name: "登录",
      path: "/login",
      component: () => import("../pages/Login.vue"),
    },
    {
      name: "404",
      path: "/:pathMatch(.*)", // 使用正则表达式查找不存在的路径
      component: () => import("../pages/404.vue"),
    },
  ];

  const indexRouter = {
    name: "index",
    path: "/index",
    component: () => import("../layout/index.vue"),
  };

  const commonRouter = [
    {
      name: "home",
      path: "/",
      component: () => import("../pages/Home.vue"),
    },
    {
      name: "我的信息",
      path: "/information",
      component: () => import("../pages/Personal/information.vue"),
    },
    {
      name: "关于",
      path: "/about",
      component: () => import("../pages/About/index.vue"),
    },
  ];

  // 管理员路由
  const adminRouter = [
    {
      name: "用户管理",
      path: "/users",
      component: () => import("../pages/User/index.vue"),
    },
    {
      name: "员工管理",
      path: "/employees",
      component: () => import("../pages/Employee/index.vue"),
    },
  ];

  // 管理员、员工路由
  const normalRouter = [
    {
      name: "首页",
      path: "/home",
      component: () => import("../pages/Home.vue"),
    },
    {
      name: "车辆信息",
      path: "/vehicles",
      component: () => import("../pages/Application/vehicle-info.vue"),
    },
    {
      name: "申请列表",
      path: "/applicationList",
      component: () => import("../pages/Application/application-list.vue"),
    },
    {
      name: "车辆轨迹",
      path: "/track",
      component: () => import("../pages/Location/vehicle-track.vue"),
    },
  ];

  // 员工路由
  const employeeRouter = [
    {
      name: "当前车辆",
      path: "/current",
      component: () => import("../pages/Location/current-vehicle.vue"),
    },
  ];

  // 注册用户路由
  const userRouter = [
    {
      name: "首页",
      path: "/home",
      component: () => import("../pages/Home2.vue")
    }
  ]

  // switch (window.$identity) {
  switch (identity) {
    case "admin":
      indexRouter.children = commonRouter
        .concat(normalRouter)
        .concat(adminRouter);
      console.log(indexRouter, "111");
      break;
    case "employee":
      indexRouter.children = commonRouter.concat(normalRouter).concat(employeeRouter);
      console.log(indexRouter, "222");
      break;
    case "user":
      indexRouter.children = userRouter.concat(commonRouter);
      console.log(indexRouter, "333");
      break;
  }

  basicRoutes.push(indexRouter);

  return basicRoutes;
}

export { getBasicRoutes };
