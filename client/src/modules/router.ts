import { createRouter, createWebHistory } from "vue-router";
import { getBasicRoutes } from "./routesData";

// 无法使用pinia，该部分代码在app.use(createPinia())还没挂载时就运行了。
// import { userStore } from '../stores/index'
// const user_store = userStore()

const userIdentity:string = localStorage.getItem("identity");
const basicRoutes = getBasicRoutes(userIdentity);
console.log("basoc", basicRoutes, window.$identity);

const router = createRouter({
  routes: basicRoutes,
  history: createWebHistory(),
});

// 路由守卫
router.beforeEach((to, from, next) => {
  // 判断本地是否存在token，登录页和注册页放行
  const isLogin = localStorage.aToken ? true : false;
  if (to.path == "/login" || to.path == "/register") {
    next();
  } else {
    isLogin ? next() : next("/login");
  }
});
export default router;
