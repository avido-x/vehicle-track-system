// 设置 关于  路由
import { RouteRecordRaw } from "vue-router";
import { CodeOutlined } from "@vicons/antd";
import { Layout } from "../constant";
import { renderIcon, renderTag } from "../../utils/common";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/about",
    name: "about",
    // component: () => import('../../pages/About/index.vue'),
    component: Layout,
    meta: {
      // sort: 10,
      isRoot: true,
      activeMenu: "about_index",
      icon: renderIcon(CodeOutlined),
    },
    children: [
      {
        path: "index",
        name: "about_index",
        meta: {
          title: "关于",
          extra: renderTag(),
          activeMenu: "about_index",
        },
        component: () => import("../../pages/About/index.vue"),
      },
    ],
  },
];
