// 个人信息
import { RouteRecordRaw } from 'vue-router'

const Information = () => import('../../pages/Personal/information.vue')
const Data = () => import('../../pages/Personal/data.vue')

const routes: Array<RouteRecordRaw> = [
    {
        path: '/personal',
        name: 'personal',
        redirect: '/personal/information',
        meta: {
            title: '个人信息',

        },
        children: [
            {
                path: 'information',
                name: 'information',
                meta: {
                    title: '我的信息'
                },
                component: Information
            },
            {
                path: 'data',
                name: 'data',
                meta: {
                    title: '数据统计'
                },
                component: Data
            }
        ]
    }
]


export default routes;