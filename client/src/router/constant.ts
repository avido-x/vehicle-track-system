// 声明常量名，导出其对应路由：方便引用
export const Layout = () => import('../layout/index.vue')

export const ErrorPage = () => import('../pages/404.vue')