// 3为用户管理 4为员工管理，即管理员界面
export const users = [
  {
    id: 1,
    name: "admin",
    auth: [2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 15],
  },
  {
    id: 2,
    name: "employee",
    auth: [2, 5, 6, 7, 8, 9, 10, 11, 12, 15],
  },
  {
    id: 3,
    name: "user",
    auth: [14, 11, 12,15],
  },
];
