import { routes } from './routesTable';
import { users } from './usersTable'
// 侧边菜单栏
import AsideMenu from './index.vue'
import { formatRouterTree, generateRouter } from '../../../utils/formatRouter'

function getMenuList() {
    let authRouter = []
    const userInfo = users.filter(user => user.name === localStorage.getItem('identity')) // 获取对应auth
    userInfo[0].auth.map(id => {
        routes.map(router => {
            if(router.id === id) {
                authRouter.push(router)  // 获取拥有权限的router
            }
        })
    })
    let menuList = formatRouterTree(authRouter)
    menuList = generateRouter(menuList)

    return menuList
}

export { AsideMenu, getMenuList }