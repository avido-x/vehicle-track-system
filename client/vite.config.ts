import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
// 自动引入naive ui
import Components from 'unplugin-vue-components/vite'
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers'
// import resolveExternalsPlugin  from 'vite-plugin-resolve-externals'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    Components({
      resolvers: [NaiveUiResolver()]
    }),
    // resolveExternalsPlugin({
    //   BMap: 'BMap',
    //   BMapGL: 'BMapGL'
    // })
  ],
  // 配置跨域
  server: {
    hmr: {
      overlay: false
    },
    host: 'localhost', 
    port: 3000,
    https: false,
    proxy: {
      "/api": {
        target: "http://localhost:5000/api/",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      }
    }
  }
})
