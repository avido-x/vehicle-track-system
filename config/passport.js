const JwtStrategy = require('passport-jwt').Strategy
const ExtractJwt = require('passport-jwt').ExtractJwt
const mongoose = require('mongoose')
const User = mongoose.model('user')
const keys = require('./keys')

// 创建opts对象，声明属性
const opts = {}
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = keys.secretOrKey

module.exports = passport => {
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
        console.log(jwt_payload);  // 用户调用接口时，控制台输出用户信息
        User.findById(jwt_payload.id)
            .then(user => {
                if(user) return done(null, user)
                else return done(null, false)
            })
            .catch(err => console.log(err))
    }))
}
