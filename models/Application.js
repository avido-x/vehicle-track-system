// 搭建车辆申请列表
// vehicle_id, plates_num, date, time, driver, driver_phone, status, application_status
// 车辆ID，车牌号，申请日期，申请时间段，申请人，申请人号码，车辆状态，申请状态
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const applicationSchema = new Schema({
    vehicle_id: {
        type: Number,
        required: true
    },
    plates_num: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    driver: {
        type: String,
        required: true
    },
    driver_phone: {
        type: Number,
        required: true
    },
    status: {
        type: Number,
        required: true
    },
    application_status: {
        type: Number,
        required: true
    }
})

const Appliction = mongoose.model('application', applicationSchema)

module.exports = Appliction