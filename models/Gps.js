// 搭建车辆GPS模型: 
// vehicle_id, plates_num, gps_lat, gps_lng, gps_date, gps_time, driver, driver_phone,path, status
// 车辆ID，车牌号，纬度，经度，日期，时间，驾驶人，驾驶人电话，路径，车辆状态
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const gpsSchema = new Schema({
    vehicle_id: {
        type: Number,
        required: true
    },  
    plates_num: {
        type: String,
        required: true
    },
    gps_lat: {
        type: Number,
        required: true
    },
    gps_lng: {
        type: Number,
        required: true
    },
    gps_date: {
        type: Date,
        required: true
    },
    gps_time: {
        type: String,
        required: true
    },
    driver: {
        type: String,
        required: true
    },
    driver_phone: {
        type: Number,
        required: true
    },
    path: {
        type: Array
    },
    status: {
        type: String,
        required: true
    }
})

const Gps = mongoose.model('gps', gpsSchema)

module.exports = Gps