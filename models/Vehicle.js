// 搭建车辆模型: vehicle_id, plates_num, model, color, route, time, origin, status
// id 车牌 车型 颜色 适用路线 可申请时间段 车辆起点 车辆状态
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const vehicleSchema = new Schema({
    vehicle_id: {
        type: Number,
        required: true
    },
    plates_num: {
        type: String,
        required: true
    },
    model: {
        type: String,
        required: true
    },
    color: {
        type: String,
        required: true
    },
    route: {
        type: String,
        required: true
    },
    time: {
        type: String,
        required: true
    },
    origin: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    }
})

const Vehicle = mongoose.model('vehicle', vehicleSchema)

module.exports = Vehicle