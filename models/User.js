// 搭建用户模型: id name email phone password age identity register_time
// 1. 引入mongoose模块，创建数据库模式
const mongoose = require('mongoose')
const Schema = mongoose.Schema

// 2. 规范数据格式
const userSchema = new Schema({
    user_id: {
        type: Number,
        // required: true
    },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    phone: {
        type: Number,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    age: {
        type: Number
    },
    identity: {
        type: String,
        default: 'user'  // 默认用户身份
    },
    register_time: {
        type: Date,
        default: Date.now
    }
})

// 3. 创建模型
const User = mongoose.model('user',userSchema)

// 4. 导出该用户模型
module.exports = User