// 1. 通过express搭建服务器
// 2. 引入mongoose模块，连接本地数据库
// 3. 引入自定义的路由模块
const express = require('express');
const mongoose = require('mongoose')
const app = express()
const users = require('./routes/api/users')
const vehicles = require('./routes/api/vehicles')
const applications = require('./routes/api/applications')
const gps = require('./routes/api/gps')
const passport = require('passport')

const db = require('./config/keys').mongoURI
mongoose.connect(db)  // 连接数据库并验证是否连接成功
    .then(() => {
        console.log('MongoDB Connected.');
    })
    .catch(err => {
        console.log('MongoDB Connected Error: ', err);
    })

const port = process.env.PORT || 5000  // 设置端口号

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
})

app.get('/', (req, res) => {
    res.send('test server running successfully.')
})

// 获取post请求参数，解析用户传参
// 注： 该操作会解析路由和请求体参数，在注册路由后引入则无法解析，则无法得到请求参数，即undefined(bug1)
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

// 初始化passport，调用自定义验证Token函数
app.use(passport.initialize())
require('./config/passport')(passport)


app.use("/api/users",users)
app.use("/api/vehicles",vehicles)
app.use("/api/applications", applications)
app.use("/api/gps", gps)